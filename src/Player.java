
public class Player {
    private boolean whiteTeam;
    private boolean firstPlayer;
    private PieceSet pieceSet;

    Player(boolean whiteTeam, boolean firstPlayer){
        this.whiteTeam = whiteTeam;
        this.firstPlayer = firstPlayer;
        this.pieceSet = new PieceSet(whiteTeam);
    }

    public boolean isFirstPlayer() {
        return firstPlayer;
    }

    public boolean isWhiteTeam() {
        return whiteTeam;
    }

    public PieceSet getPieceSet() {
        return pieceSet;
    }

    public void addPiecesToBoard(Board gameBoard){
        int pawnOffset = 0;
        int royalsOffset = 0;

        if(!firstPlayer){ // Offsets to set the position of the pieces at the top of the board
            pawnOffset = -5;
            royalsOffset = -7;
        }
        int royalPosI = 7 + royalsOffset;
        //Initializes King
        pieceSet.getPieces()[0].initialize(gameBoard.getSquares()[royalPosI][4]);
        //Initializes Queen
        pieceSet.getPieces()[1].initialize(gameBoard.getSquares()[royalPosI][3]);
        //Initializes Bishops
        pieceSet.getPieces()[2].initialize(gameBoard.getSquares()[royalPosI][2]);
        pieceSet.getPieces()[3].initialize(gameBoard.getSquares()[royalPosI][5]);
        //Initializes Knights
        pieceSet.getPieces()[4].initialize(gameBoard.getSquares()[royalPosI][1]);
        pieceSet.getPieces()[5].initialize(gameBoard.getSquares()[royalPosI][6]);

        // Initializes Rooks
        pieceSet.getPieces()[6].initialize(gameBoard.getSquares()[royalPosI][0]);
        pieceSet.getPieces()[7].initialize(gameBoard.getSquares()[royalPosI][7]);

        // Initializes Pawns
        for(int i = 0; i < 8; i++){
            pieceSet.getPieces()[8 + i].initialize(gameBoard.getSquares()[6 + pawnOffset][i]);
        }
        // Adds the respective pieces' ImageView nodes to their square's StackPanes.
//        for(int i = 0; i < pieceSet.getPieces().length; i++){
//            pieceSet.getPieces()[i].getPaneChildren().add(pieceSet.getPieces()[i].getImgNode());
//        }
    }

    public String getTeam(){
        if(whiteTeam){
            return "white";
        }
        else{
            return "black";
        }
    }

    @Override
    public String toString(){
        String color;
        if(whiteTeam){
            color = "WHITE";
        }
        else{
            color = "BLACK";
        }
        return (color + " player");
    }
}
