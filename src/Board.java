import javafx.animation.FillTransition;
import javafx.animation.Timeline;
import javafx.geometry.Pos;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.RowConstraints;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;
import javafx.util.Duration;

import java.util.Arrays;
import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;

public class Board {
    private Square[][] squareArray;
    private GridPane gameBoard;
    private Turn turn;

    Board(int width, int height, Turn turn){
        this.squareArray = new Square[height][width];
        for(int i = 0; i < squareArray.length; i++){
            for(int j = 0; j < squareArray[i].length; j++){
                Color color;
                if((j + i) % 2 == 0){
                    color = Color.CHOCOLATE.darker();
                }
                else{
                    color = Color.BURLYWOOD;
                }
                squareArray[i][j] = new Square(color, i, j);
            }
        }
        this.turn = turn;
        initBoard();
        //TODO: NOTE: moveTest() will not work properly if showIJ() is still active and vice versa
//        showIJ(); //TODO: Remove this method when done.
        moveTest(); //TODO: Remove this method when done. 0
    }

    public Turn getTurn() {
        return turn;
    }

    public Square[][] getSquares(){
        return squareArray;
    }

    /**
     * Adds the i and j indices of the respective square on the board that the mouse is hovering over
     */
    public void showIJ(){
        for(int i = 0; i < squareArray.length; i++){
            for(int j = 0; j < squareArray[i].length; j++){
                Square s = squareArray[i][j];

                s.squarePane.setOnMouseEntered(event -> {
                    Text text = new Text("i: " + s.position[1] + "  j: " + s.position[0]);
                    s.squarePane.getChildren().addAll(text);

                });

                s.squarePane.setOnMouseExited(event -> {
                    s.squarePane.getChildren().remove(s.squarePane.getChildren().size() - 1);
                });

            }
        }
    }


    /**
     * Prints out the status of the square on the board. Prints true if occupied and the Piece object that is
     * occupying the square.
     */
    public void moveTest(){
        AtomicReference<String> msg = new AtomicReference<>("");
        AtomicBoolean isPieceSelected = new AtomicBoolean(false); //Is true if a piece has been selected
        AtomicReference<Piece> activePiece = new AtomicReference<>(); // Selected Piece
        for(int i = 0; i < squareArray.length; i++) {
            for (int j = 0; j < squareArray[i].length; j++) {
                Square s = squareArray[i][j];
                ArrayList<Square> legalSquares = new ArrayList<>(0);

                //Event for animating the square when a piece is selected
                s.squarePane.setOnMouseClicked(clickEv ->{
                    if(s.isOccupied()) {

                        //Logic for legal king movement
                        if(isPieceSelected.get() && activePiece.get().getName().equals("KING")){
                            ArrayList<int[]> kingMoves = activePiece.get().getLegalMoves();
                            boolean legalKingMove = false;
                            for(int[] z : kingMoves){
                                if(Arrays.equals(s.getPosition(), z)){
                                    legalKingMove = true;
                                    break;
                                }
                            }
                            if(legalKingMove){
                                if(!s.getOccupyingPiece().getTeam().equals(activePiece.get().getTeam())){
                                    msg.set(activePiece.get().toString() + " has captured " + s.getOccupyingPiece().toString());
                                    //adds a message to the text field
                                    turn.getMessage().setText(msg.get());

                                    s.removePiece();
                                    s.getSquarePane().getChildren().remove(s.getSquarePane().getChildren().size() - 1);
                                    activePiece.get().moveTo(s);
                                    activePiece.set(null);
                                    isPieceSelected.set(false);
                                    this.turn.pieceMoved_True(); //piece has been moved, further piece movement for this turn is prohibited

                                }
                                else{
                                    msg.set("You can't move onto other black pieces");
                                    //adds a message to the text field
                                    turn.getMessage().setText(msg.get());
                                }
                            }

                            else if(Arrays.equals(activePiece.get().getSquare().getPosition(), s.getPosition())){
                                s.endAnimations();
                                msg.set(s.getOccupyingPiece().toString() + " has been deselected.");
                                //adds a message to the text field
                                turn.getMessage().setText(msg.get());

                                activePiece.set(null);
                                isPieceSelected.set(false);
                            }
                            else{
                                msg.set("Illegal King Move");
                                turn.getMessage().setText(msg.get());
                            }
                        }
                        //Selecting pieces on first click, and interacting with pieces that aren't the king
                        else{
                            //Logic for when a white piece captures a black piece, or vice versa
                            if(isPieceSelected.get() && !s.getOccupyingPiece().getTeam().equals(activePiece.get().getTeam())){

                                msg.set(activePiece.get().toString() + " has captured " + s.getOccupyingPiece().toString());
                                //adds a message to the text field
                                turn.getMessage().setText(msg.get());

                                s.removePiece();
                                s.getSquarePane().getChildren().remove(s.getSquarePane().getChildren().size() - 1);
                                activePiece.get().moveTo(s);
                                activePiece.set(null);
                                isPieceSelected.set(false);
                                this.turn.pieceMoved_True(); //piece has been moved, further piece movement for this turn is prohibited
                            }

                            //Logic to set a selected piece if it is the first time it is clicked
                            else if(!isPieceSelected.get() && !this.turn.isPieceMoved()) {
                                if (s.occupyingPiece.getTeam().equals(this.turn.getCurrentPlayer().getTeam())){
                                    s.animateMoves();
                                    isPieceSelected.set(true);
                                    activePiece.set(s.getOccupyingPiece());
                                    msg.set(s.getOccupyingPiece().toString() + " " + Arrays.toString(s.position)
                                            + " as been selected");
                                    //adds a message to the text field
                                    turn.getMessage().setText(msg.get());

                                }

                            }
                            //Logic for if the same piece is clicked twice. Doing so will un-select the piece
                            else {
                                s.endAnimations();
                                if(activePiece.get() != null){ // This if statement was added to stop a NullPointerException thrown when trying to select a piece after moving a piece.
                                    if(activePiece.get().equals(s.getOccupyingPiece())) {

                                        msg.set(s.getOccupyingPiece().toString() + " has been deselected.");
                                        //adds a message to the text field
                                        turn.getMessage().setText(msg.get());
                                        activePiece.set(null);
                                        isPieceSelected.set(false);
                                    }
                                }
                            }
                        }
                    }

                    //Moves the selected piece to a new square on the chess board
                    else if(!s.isOccupied() && isPieceSelected.get()) {
                        //Logic for legal king movement
                        if (isPieceSelected.get() && activePiece.get().getName().equals("KING")) {
                            ArrayList<int[]> kingMoves = activePiece.get().getLegalMoves();
                            boolean legalKingMove = false;
                            for (int[] z : kingMoves) {
                                if (Arrays.equals(s.getPosition(), z)) {
                                    legalKingMove = true;
                                    break;
                                }
                            }
                            //Move selected is a valid move for the King and the selected king is moved
                            if (legalKingMove) {
                                activePiece.get().moveTo(s);
                                activePiece.set(null);
                                isPieceSelected.set(false);
                                this.turn.pieceMoved_True(); //piece has been moved, further piece movement for this turn is prohibited
                            }
                            //King cannot move to that square
                            else {
                                msg.set("Illegal King Move");
                                //adds a message to the text field
                                turn.getMessage().setText(msg.get());
                            }
                        }
                        //Logic for pieces that are not a king
                        else {
                            activePiece.get().moveTo(s);
                            msg.set(s.getOccupyingPiece().toString() + " has been moved to " +
                                     Arrays.toString(s.position));
                            //adds a message to the text field
                            turn.setMessage(msg.get());
                            activePiece.set(null);
                            isPieceSelected.set(false);
                            this.turn.pieceMoved_True(); //piece has been moved, further piece movement for this turn is prohibited
                        }

                    }
                });
                //Ends the animation of the Square after the piece has been moved
                s.r.fillProperty().addListener(fillList ->{
                    if(!s.isOccupied()){
                        s.endAnimations();
                        for(Square validMove: legalSquares){
                            validMove.endAnimations();
                        }
                    }
                });

            }
        }
    }

    private void initBoard(){
        this.gameBoard = new GridPane();
        gameBoard.setAlignment(Pos.CENTER);
        for(int i = 0; i < squareArray.length; i++) {
            gameBoard.getRowConstraints().add(new RowConstraints(100));
            gameBoard.getColumnConstraints().add(new ColumnConstraints(100));
            for (int j = 0; j < squareArray[i].length; j++) {
                gameBoard.add(squareArray[i][j].getSquarePane(), j, i);
            }
        }

    }

    public GridPane getGameBoard(){
        return this.gameBoard;
    }

    public static class Square {
        private boolean occupied;
        private Color color;
        private StackPane squarePane;
        private Piece occupyingPiece;
        private int[] position; // TODO: Possible place for bugs
        private Rectangle r;
        private FillTransition goBlink;


        Square(Color color, int iIndex, int jIndex){ // TODO: Possible place for bugs
            this.occupied = false;
            this.color = color;
            this.r = new Rectangle(100, 100);
            this.squarePane = initSquare();
            this.occupyingPiece = null;
            position = new int[]{jIndex, iIndex}; // TODO: Possible place for bugs
        }

        /*
        Initializes the square. The square consists of a stackPane, which holds a Rectangle Object
         */
        private StackPane initSquare(){
            this.r.setFill(this.color);
            this.r.setStroke(Color.TRANSPARENT);
            StackPane sp = new StackPane();
            sp.getChildren().addAll(this.r);
            this.goBlink = new FillTransition(Duration.millis(700), r);
            return sp;
        }

        public int[] getPosition(){
            return this.position;
        }

        public boolean isOccupied(){
            return this.occupied;
        }

        /**
         * Adds a specified Piece Object to the square. Used for moving pieces on the board.
         * @param piece Piece object that will be added to the Stack Pane
         */
        public void addPiece(Piece piece){
            this.occupied = true;
            this.occupyingPiece = piece;
            this.squarePane.getChildren().add(piece.getImgNode());
        }
        public Piece getOccupyingPiece(){
            return this.occupyingPiece;
        }

        public void animateMoves(){
            goBlink.setCycleCount(Timeline.INDEFINITE);
            goBlink.setFromValue(this.color);
            goBlink.setToValue(Color.LIGHTBLUE);
            goBlink.play();
        }

        public void animateAttackMove(){
            goBlink.setCycleCount(Timeline.INDEFINITE);
            goBlink.setFromValue(this.color);
            goBlink.setToValue(Color.RED);
            goBlink.play();
        }

        public void endAnimations(){
            goBlink.stop();
            this.r.setFill(this.color);
        }

        public void removePiece(){
            this.occupied = false;
            this.occupyingPiece = null;
            r.setFill(this.color);
        }

        public StackPane getSquarePane(){
            return this.squarePane;
        }
    }
}
