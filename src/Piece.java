import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.*;

public abstract class Piece{
    protected boolean whiteTeam;
    protected ImageView imgNode;
    private Board.Square boardSquare; // Holds reference to the respective square on the board that the piece occupies
    private boolean initialized; // True if the piece holds a reference to its boardSquare

    public String getTeam() {
        if(whiteTeam){
            return "white";
        }
        else{
            return "black";
        }
    }

    @Override
    public String toString(){
        return (this.getTeam() + " " + this.getName());
    }

    public void initialize(Board.Square boardSquare){
        this.boardSquare = boardSquare;
        this.boardSquare.addPiece(this);
        this.initialized = true;
    }

    public boolean isInitialized(){
        return this.initialized;
    }

    public void moveTo(Board.Square boardSquare){
        this.boardSquare.removePiece();

        this.boardSquare = boardSquare;
        this.boardSquare.addPiece(this);
    }

    public ArrayList<int[]> getLegalMoves(){
        ArrayList<int[]> moves = new ArrayList<>();
        return moves;
    }

    public Board.Square getSquare() {
        return boardSquare;
    }

    public ObservableList<Node> getPaneChildren(){
        return this.boardSquare.getSquarePane().getChildren();
    }

    public abstract String getName();

    protected abstract void createImg();

    public ImageView getImgNode() {
        this.imgNode.setPreserveRatio(true);
        this.imgNode.setFitWidth(100);
        this.imgNode.setFitHeight(100);
        return imgNode;
    }
}

class Pawn extends Piece{

    Pawn(boolean whiteTeam){
        this.whiteTeam = whiteTeam;
        createImg();
    }

    @Override
    public ArrayList<int[]> getLegalMoves(){
        ArrayList<int[]> moves = new ArrayList<>();
        if(this.whiteTeam){
            int[] location = this.getSquare().getPosition();
//            System.out.println(location[0] + " " + location[1]);
            int[] move1 = {location[0], location[1]+1};
//            System.out.println(move1[0] + " " + move1[1]);
            int[] move2 = {location[0]-1, location[1]+1};
            int[] move3 = {location[0]+1, location[1]+1};

            moves.add(move1);
//            System.out.println(moves.get(0)[0] + " " + moves.get(0)[1]);
            moves.add(move2);
            moves.add(move3);


        }
        else{
            int[] location = this.getSquare().getPosition();
            int[] move1 = {location[0], location[1]-1};
            int[] move2 = {location[0]-1, location[1]-1};
            int[] move3 = {location[0]+1, location[1]-1};

            moves.add(move1);
            moves.add(move2);
            moves.add(move3);
        }

        return moves;
    }

    @Override
    public String getName() {
        return "PAWN";
    }

    @Override
    protected void createImg() {
        String team;
        if(whiteTeam){
            team = "white";
        }
        else{
            team = "black";
        }
        try {
            FileInputStream in = new FileInputStream(System.getProperty("user.dir") +
                    "\\Piece_assets\\" + team + "\\pawn.png");
            this.imgNode = new ImageView(new Image(in));
        }
        catch(FileNotFoundException ex){
            System.out.println("Could find image for: " + team + " " + getName());
            ex.printStackTrace();
        }
    }
}

class Queen extends Piece{

    Queen(boolean whiteTeam){
        this.whiteTeam = whiteTeam;
        createImg();
    }

    @Override
    public String getName() {
        return "QUEEN";
    }

    @Override
    protected void createImg() {
        String team;
        if(whiteTeam){
            team = "white";
        }
        else{
            team = "black";
        }
        try {
            FileInputStream in = new FileInputStream(System.getProperty("user.dir") +
                    "\\Piece_assets\\" + team + "\\queen.png");
            this.imgNode = new ImageView(new Image(in));
        }
        catch(FileNotFoundException ex){
            System.out.println("Could find image for: " + team + " " + getName());
            ex.printStackTrace();
        }
    }

}

class King extends Piece{

    King(boolean whiteTeam){
        this.whiteTeam = whiteTeam;
        createImg();
    }

    @Override
    public ArrayList<int[]> getLegalMoves(){
        ArrayList<int[]> moves = new ArrayList<>();

        int[] location = this.getSquare().getPosition();
        int[] move1 = {location[0], location[1]+1};
        int[] move2 = {location[0]-1, location[1]+1};
        int[] move3 = {location[0]+1, location[1]+1};
        int[] move4 = {location[0], location[1]-1};
        int[] move5 = {location[0]-1, location[1]-1};
        int[] move6 = {location[0]+1, location[1]-1};
        int[] move7 = {location[0]+1, location[1]};
        int[] move8 = {location[0]-1, location[1]};

        moves.add(move1);
        moves.add(move2);
        moves.add(move3);
        moves.add(move4);
        moves.add(move5);
        moves.add(move6);
        moves.add(move7);
        moves.add(move8);




        return moves;
    }

    @Override
    public String getName() {
        return "KING";
    }

    @Override
    protected void createImg() {
        String team;
        if(whiteTeam){
            team = "white";
        }
        else{
            team = "black";
        }
        try {
            FileInputStream in = new FileInputStream(System.getProperty("user.dir") +
                    "\\Piece_assets\\" + team + "\\king.png");
            this.imgNode = new ImageView(new Image(in));
        }
        catch(FileNotFoundException ex){
            System.out.println("Could find image for: " + team + " " + getName());
            ex.printStackTrace();
        }
    }
}

class Rook extends Piece{

    Rook(boolean whiteTeam){
        this.whiteTeam = whiteTeam;
        createImg();
    }

    @Override
    public String getName() {
        return "ROOK";
    }

    @Override
    protected void createImg() {
        String team;
        if(whiteTeam){
            team = "white";
        }
        else{
            team = "black";
        }
        try {
            FileInputStream in = new FileInputStream(System.getProperty("user.dir") +
                    "\\Piece_assets\\" + team + "\\rook.png");
            this.imgNode = new ImageView(new Image(in));
        }
        catch(FileNotFoundException ex){
            System.out.println("Could find image for: " + team + " " + getName());
            ex.printStackTrace();
        }
    }
}

class Bishop extends Piece{

    Bishop(boolean whiteTeam){
        this.whiteTeam = whiteTeam;
        createImg();
    }

    @Override
    public String getName() {
        return "BISHOP";
    }

    @Override
    protected void createImg() {
        String team;
        if(whiteTeam){
            team = "white";
        }
        else{
            team = "black";
        }
        try {
            FileInputStream in = new FileInputStream(System.getProperty("user.dir") +
                    "\\Piece_assets\\" + team + "\\bishop.png");
            this.imgNode = new ImageView(new Image(in));
        }
        catch(FileNotFoundException ex){
            System.out.println("Could find image for: " + team + " " + getName());
            ex.printStackTrace();
        }
    }
}

class Knight extends Piece{

    Knight(boolean whiteTeam){
        this.whiteTeam = whiteTeam;
        createImg();
    }

    @Override
    public String getName() {
        return "KNIGHT";
    }

    @Override
    protected void createImg() {
        String team;
        if(whiteTeam){
            team = "white";
        }
        else{
            team = "black";
        }
        try {
            FileInputStream in = new FileInputStream(System.getProperty("user.dir") +
                    "\\Piece_assets\\" + team + "\\knight.png");
            this.imgNode = new ImageView(new Image(in));
        }
        catch(FileNotFoundException ex){
            System.out.println("Could find image for: " + team + " " + getName());
            ex.printStackTrace();
        }
    }
}
