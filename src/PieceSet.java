import java.util.Arrays;

public class PieceSet {
    private Piece[] pieces = new Piece[16];

    PieceSet(boolean whiteTeam){
        for (int i = 0; i < pieces.length; i++){
            switch (i){
                case 0: pieces[i] = new King(whiteTeam); break;
                case 1: pieces[i] = new Queen(whiteTeam); break;
                case 2:
                case 3: pieces[i] = new Bishop(whiteTeam); break;
                case 4:
                case 5: pieces[i] = new Knight(whiteTeam); break;
                case 6:
                case 7: pieces[i] = new Rook(whiteTeam); break;
                case 8:
                case 9:
                case 10:
                case 11:
                case 12:
                case 13:
                case 14:
                case 15: pieces[i] = new Pawn(whiteTeam); break;
            }
        }

//        System.out.println(Arrays.toString(pieces));
    }

    public Piece[] getPieces() {
        return pieces;
    }
}
