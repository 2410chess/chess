import javafx.scene.text.Text;

public class Turn {
    private Player[] players; //Array of all the players in the game
    private Player currentPlayer; //Player whose turn it is in the game
    private boolean turnActive; //When false, currentPlayer switches to the other player
    private boolean pieceMoved; //When false, allows the currentPlayer to move their piece
    private Text message; //Message for what is going on

    Turn(Player[] players){
        this.players = players;
        message = new Text("Turn initialized");
    }

    public Player getCurrentPlayer(){
        return this.currentPlayer;
    }

    public boolean isPieceMoved() {
        return pieceMoved;
    }

    public void setCurrentPlayer(int playerNum){
        if(playerNum > players.length - 1){
            throw(new InvalidPlayerNumber(playerNum));
        }
        this.currentPlayer = players[playerNum];
        this.pieceMoved = false;
        this.turnActive = true;
        message.setText("It is now " + this.currentPlayer.toString() + "'s turn.");

        //TODO: for testing
//        System.out.println("It is now " + this.currentPlayer.toString() + "'s turn.");
    }
    public Text getMessage(){
        message.setText("It is now " + this.currentPlayer.toString() + "'s turn.");
        return  message;
    }
    public void setMessage(String msg){
        message.setText(msg);
    }

    //Updates whose turn it is. Currently only meant for 2 player games
    public void endTurn(){
        if(currentPlayer == players[0]){
            currentPlayer = players[1];
        }
        else if(currentPlayer == players[1]){
            currentPlayer = players[0];
        }
        else{
            System.out.println("Not sure what you did but the currentPlayer is not valid.");
            throw new UnknownError();
        }
//        System.out.println("It is now " + currentPlayer.toString() + "'s turn.");
        pieceMoved = false;
        message.setText("It is now " + this.currentPlayer.toString() + "'s turn.");
    }

    public void pieceMoved_True(){
        pieceMoved = true;
        //TODO: testing
//        System.out.println(currentPlayer.toString() + " just moved a piece.");
    }

    static class InvalidPlayerNumber extends Error{
        private int num;

        InvalidPlayerNumber(int num){
            this.num = num;
        }
        @Override
        public String getMessage() {
            return ("\"" + num + "\"" + " is an invalid player number.");
        }
    }

}
