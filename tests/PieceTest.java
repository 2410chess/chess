import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

public class PieceTest extends Application {
    public void start(Stage st) {
        HBox p = new HBox();


        Pawn pawn = new Pawn(true);
        Queen queen = new Queen(true);
        King king = new King(true);
        Knight knight = new Knight(true);
        Rook rook = new Rook(true);
        Bishop bishop = new Bishop(true);

        Pawn pawn1 = new Pawn(false);
        Queen queen1 = new Queen(false);
        King king1 = new King(false);
        Knight knight1 = new Knight(false);
        Rook rook1 = new Rook(false);
        Bishop bishop1 = new Bishop(false);

        p.getChildren().addAll(pawn.getImgNode(), queen.getImgNode(),
                king.getImgNode(), knight.getImgNode(), rook.getImgNode(),
                bishop.getImgNode(), pawn1.getImgNode(), queen1.getImgNode(),
                king1.getImgNode(), knight1.getImgNode(), rook1.getImgNode(),
                bishop1.getImgNode());



        Scene sc = new Scene(p, 650, 250);
        sc.setFill(Color.MAROON);
        st.setScene(sc);
        st.show();
    }
}
