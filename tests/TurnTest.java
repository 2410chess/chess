import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.control.*;
import javafx.scene.layout.BorderPane;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.scene.layout.*;
import javafx.scene.text.*;
import javafx.stage.Stage;
import javafx.scene.control.TextField;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.*;

public class TurnTest extends Application {


    public void start(Stage st){
        BorderPane bp = new BorderPane();
        Scene sc = new Scene(bp, 1200, 900);

        Player p1 = new Player(true, false);
        Player p2 = new Player(false, true);

        Turn turn = new Turn(new Player[]{p1, p2});
        turn.setCurrentPlayer(1); //Sets the black player to be first player

        Board board = new Board(8 , 8,  turn);

        bp.setCenter(board.getGameBoard());
        bp.setPrefHeight(800);
        bp.setPrefWidth(800);

        HBox h = new HBox();
        h.setAlignment(Pos.CENTER);

        //Sets the text field to the top of the borderPane
        TextField notice = new TextField();
        notice.setText("I am a text field");
        notice.setFont(Font.font("Verdana", FontWeight.BOLD, 20));

        //Binds the notice message with the Turn() message in Board().
        notice.textProperty().bind(board.getTurn().getMessage().textProperty());

        notice.setAlignment(Pos.CENTER);

        notice.setMinWidth(600);
        notice.setPrefWidth(600);
        notice.setMaxWidth(600);

        notice.setMinHeight(50);
        notice.setPrefHeight(50);
        notice.setMaxHeight(50);

        h.getChildren().addAll(notice);
        bp.setTop(h);


        VBox vLeft = new VBox();
        vLeft.setAlignment(Pos.CENTER);
        vLeft.setSpacing(50);
        vLeft.setPadding(new Insets(15, 15, 15, 15));

        CheckBox inCheck = new CheckBox("Check");
        CheckBox inCheckmate = new CheckBox("Checkmate");

        inCheck.selectedProperty().addListener(fillList ->{
            if(board.getTurn().getCurrentPlayer().getTeam().equals("white") && inCheck.isSelected()){
                board.getTurn().getMessage().setText("Black player is in Check!");
            }
            else if(board.getTurn().getCurrentPlayer().getTeam().equals("black") && inCheck.isSelected()){
                board.getTurn().getMessage().setText("White player is in Check!");
            }
        });
        inCheckmate.selectedProperty().addListener(fillList ->{
            if(board.getTurn().getCurrentPlayer().getTeam().equals("white")){
                board.getTurn().getMessage().setText("Black player is in Checkmate!  White WINS!");
            }
            else if(board.getTurn().getCurrentPlayer().getTeam().equals("black")){
                board.getTurn().getMessage().setText("White player is in Checkmate!  Black WINS!");
            }
        });

        HBox hLeft = new HBox();
        hLeft.setAlignment(Pos.CENTER);
        hLeft.setSpacing(15);

        hLeft.getChildren().addAll(inCheck, inCheckmate);

        VBox userBox = new VBox();
        userBox.setSpacing(5);

        VBox chatBox = new VBox();
        chatBox.setSpacing(15);

        ArrayList<String> chatList = new ArrayList<>();
        TextField chat = new TextField("Type a message!");
        chat.setFont(Font.font("Verdana", 12));
        TextArea chatLog = new TextArea();
        chatLog.setFont(Font.font("Verdana",12));

        chat.setMinWidth(200);
        chat.setPrefWidth(200);
        chat.setMaxWidth(200);
        chat.setMinHeight(30);
        chat.setPrefHeight(30);
        chat.setMaxHeight(30);

        chatLog.setMinWidth(200);
        chatLog.setPrefWidth(200);
        chatLog.setMaxWidth(200);
        chatLog.setMinHeight(500);
        chatLog.setPrefHeight(500);
        chatLog.setMaxHeight(500);

        Button sendMessage = new Button("Send Message");
        sendMessage.setOnMouseClicked(sendEvent ->{
            chatList.add(chat.getText());
            chatLog.appendText("\n> " + chat.getText());
        });

        Button endTurnButton = new Button();
        endTurnButton.setText("End Turn");
        endTurnButton.setOnMouseClicked(endTurnEvent ->{
            board.getTurn().endTurn();
            inCheck.setSelected(false);
        });
        
        userBox.getChildren().addAll(chat, sendMessage);
        chatBox.getChildren().addAll(userBox, chatLog);
        vLeft.getChildren().addAll(hLeft, endTurnButton, chatBox);

        bp.setLeft(vLeft);

        p1.addPiecesToBoard(board);
        p2.addPiecesToBoard(board);

        st.setScene(sc);
        st.setTitle("Chess");

        //Sets the icon for the window
        try {
            FileInputStream in = new FileInputStream(System.getProperty("user.dir") +
                    "\\Piece_assets\\white\\king.png");
            st.getIcons().add(new Image(in));
        }
        catch(FileNotFoundException ex){
            System.out.println("Couldn't find image for: ICON");
            ex.printStackTrace();
        }

        st.show();
    }

}
